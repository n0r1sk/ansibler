/*
Copyright 2017 Mario Kleinsasser, Bernhard Rausch and Martin Steinwender

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func readBody(r *http.Request, v interface{}, emptyBodyAllowed bool) error {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	log.Debugf("%s", body)
	if len(body) == 0 && emptyBodyAllowed {
		return nil
	}

	jsonerr := json.Unmarshal(body, v)
	if jsonerr != nil {
		return jsonerr
	}

	return nil
}

func writeJSON(jsonObj interface{}, w http.ResponseWriter) {
	json, err := json.Marshal(jsonObj)
	if err != nil {
		writeError(err, w)
	} else {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, fmt.Sprintf("%s", json))
	}

}

func writeError(err error, w http.ResponseWriter) {
	log.Error(err)
	fmt.Fprint(w, err)
}
