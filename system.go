/*
Copyright 2017 Mario Kleinsasser, Bernhard Rausch and Martin Steinwender

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"sort"

	log "github.com/sirupsen/logrus"
)

type cmdResult struct {
	out []byte
	err error
}

func doCommand(basedir string, playbookPath string, playbookRegex string, playbook string, args []string) (string, error) {
	re := regexp.MustCompile(playbookRegex)
	if re.MatchString(playbook) {
		path := fmt.Sprintf("%s%s", basedir, playbookPath)
		log.Debug(path)

		result := make(chan cmdResult)
		go func() {
			chdirErr := os.Chdir(path)

			if chdirErr != nil {
				result <- cmdResult{out: []byte{}, err: chdirErr}
			}
			var cmd *exec.Cmd
			if args == nil || len(args) == 0 {
				cmd = exec.Command(fmt.Sprintf("./%s", playbook))
			} else {
				cmd = exec.Command(fmt.Sprintf("./%s", playbook), args...)
			}

			output, err := cmd.CombinedOutput()
			if err != nil {
				result <- cmdResult{out: []byte{}, err: err}
			}

			result <- cmdResult{out: output, err: nil}
		}()

		cmdResult := <-result

		return fmt.Sprintf("%s", cmdResult.out), cmdResult.err
	}
	return "Not an allowed script", nil

}

func listFiles(basedir string, folder string, playbookRegex string) []string {

	path := fmt.Sprintf("%s%s", basedir, folder)
	log.Debug(path)

	var files []string
	f, err := os.Open(path)
	if err != nil {
		log.Error(err)
		return []string{}
	}
	fileInfo, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		log.Error(err)
		return []string{}
	}

	re := regexp.MustCompile(playbookRegex)
	for _, file := range fileInfo {
		if re.MatchString(file.Name()) {
			files = append(files, file.Name())
		}
	}
	sort.Strings(files)
	return files
}
