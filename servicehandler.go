/*
Copyright 2017 Mario Kleinsasser, Bernhard Rausch and Martin Steinwender

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"path/filepath"
	"strings"

	casbin "github.com/casbin/casbin"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

type protectedInput struct {
	ProtectedSubfolder string   `json:"protectedsubfolder"`
	Playbook           string   `json:"playbook"`
	Arguments          []string `json:"arguments"`
}

type simpleExecuteInput struct {
	Arguments []string `json:"arguments"`
}

type output struct {
	Content string `json:"content"`
}

type fileList struct {
	Content []string `json:"content"`
}

func buildRouter() *mux.Router {
	r := mux.NewRouter()
	r.Methods("POST").PathPrefix("/protected").HandlerFunc(handleProtectedCommand)
	r.Methods("POST").PathPrefix("/").HandlerFunc(handleConsumerCommand)
	return r
}

func handleProtectedCommand(w http.ResponseWriter, r *http.Request) {
	if checkACL(r) {
		input := protectedInput{ProtectedSubfolder: "", Playbook: ""}
		err := readBody(r, &input, false)
		if err == nil {
			if input.Playbook == "" {
				folder := fmt.Sprintf("protected/%s", input.ProtectedSubfolder)
				listFolder(folder, w)
			} else {

				path := fmt.Sprintf("protected/%s", input.ProtectedSubfolder)
				args, argErr := handleArguments(input.Arguments)
				if argErr == nil {
					executeCommand(path, input.Playbook, args, w)
				} else {
					log.Error(argErr)
					jsonObj := output{Content: fmt.Sprint(argErr)}
					writeJSON(jsonObj, w)
				}
			}
		} else {
			log.Error(err)
			jsonObj := output{Content: fmt.Sprint(err)}
			writeJSON(jsonObj, w)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
	}
}

func handleConsumerCommand(w http.ResponseWriter, r *http.Request) {

	if checkACL(r) {
		if strings.HasSuffix(r.URL.Path, ".sh") {
			path, playbook := filepath.Split(r.URL.Path[1:])

			input := simpleExecuteInput{}
			err := readBody(r, &input, true)
			if err == nil {

				args, argErr := handleArguments(input.Arguments)
				if argErr == nil {
					executeCommand(path, playbook, args, w)
				} else {
					log.Error(argErr)
					jsonObj := output{Content: fmt.Sprint(argErr)}
					writeJSON(jsonObj, w)
				}
			} else {

				log.Error(err)
				jsonObj := output{Content: fmt.Sprint(err)}
				writeJSON(jsonObj, w)
			}

		} else {
			folder := r.URL.Path[1:]
			listFolder(folder, w)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
	}

}

func listFolder(folder string, w http.ResponseWriter) {
	var files []string
	log.Debugf("Folder: %s", folder)
	files = listFiles(agentConfig.Playbookbasepath, folder, agentConfig.Playbookregex)
	jsonObj := fileList{Content: files}
	writeJSON(jsonObj, w)
}

func executeCommand(path string, playbook string, args []string, w http.ResponseWriter) {
	log.Debugf("Path: '%s'  File: '%s'", path, playbook)

	out, err := doCommand(agentConfig.Playbookbasepath, path, agentConfig.Playbookregex, playbook, args)
	if err == nil {
		jsonObj := output{Content: out}

		writeJSON(jsonObj, w)
	} else {
		log.Error(err)
		jsonObj := output{Content: fmt.Sprint(err)}
		writeJSON(jsonObj, w)
	}
}

func handleArguments(args []string) ([]string, error) {
	result := []string{}
	if args != nil {
		for i := range args {
			b, err := base64.StdEncoding.DecodeString(args[i])
			if err != nil {
				return nil, err
			}
			result = append(result, fmt.Sprintf("%s", b))

		}
	}
	log.Debugf("%v", result)
	return result, nil
}

func checkACL(r *http.Request) bool {
	sub := r.Header.Get("apikey")
	obj := r.URL.Path
	act := r.Method
	log.Debugf("%s  -  %s  -  %s", sub, obj, act)

	e := casbin.NewEnforcer(agentConfig.Casbinmodelfile, agentConfig.Casbinpolicyfile)
	return e.Enforce(sub, obj, act)
}
