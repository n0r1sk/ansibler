/*
Copyright 2017 Mario Kleinsasser, Bernhard Rausch and Martin Steinwender

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	gocron "github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

const defaultRegex = `(?m)api-.*\.sh`

type agentConfigObj struct {
	configFilename string
	Address        string
	Port           int
	Certfile       string
	Keyfile        string
	certMD5        string
	portOverride   int

	Casbinmodelfile  string
	Casbinpolicyfile string

	Playbookbasepath string
	Playbookregex    string
}

var (
	app      = kingpin.New("ansibler", "A command-line ansible playbook agent.")
	appDebug = app.Flag("debug", "Enable debug mode.").Bool()

	serverCommand  = app.Command("server", "Enable sever mode.")
	configFilename = serverCommand.Arg("config", "Configuration filename.").Required().String()
	serverArgPort  = serverCommand.Arg("port", "Server port").Default("443").Int()

	execCommand               = app.Command("execute", "Execute a command.")
	execArgumentPlaybookPath  = execCommand.Arg("playbookPath", "Path to the playbook.").Required().String()
	execArgumentPlaybook      = execCommand.Arg("playbook", "Playbook to be executed.").Required().String()
	execArgumentPlaybookArgs  = execCommand.Arg("playbookargs", "Playbook arguments (commaseparated).").Default("").String()
	execArgumentPlaybookRegex = execCommand.Arg("regex", "Playbook filter regex").Default(defaultRegex).String()

	listCommand               = app.Command("list", "List playbookfiles.")
	listArgumentPlaybookPath  = listCommand.Arg("playbookPath", "Path to the playbooks.").Required().String()
	listArgumentPlaybookRegex = listCommand.Arg("regex", "Playbook filter regex").Default(defaultRegex).String()

	httpServerInstance *http.Server
	agentConfig        *agentConfigObj
)

func main() {

	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)

	kpArgs := kingpin.MustParse(app.Parse(os.Args[1:]))

	if *appDebug == true {
		log.SetLevel(log.DebugLevel)
	}

	switch kpArgs {
	case "server":
		go startServer(*configFilename, *serverArgPort)

		if *appDebug == true {
			gocron.Every(30).Seconds().Do(checkServer)
		} else {
			gocron.Every(60).Minutes().Do(checkServer)
		}
		<-gocron.Start()
	case "execute":
		args := []string{}

		if *execArgumentPlaybookArgs != "" {
			args = strings.Split(*execArgumentPlaybookArgs, ",")
		}

		result, err := doCommand("", *execArgumentPlaybookPath, *execArgumentPlaybookRegex, *execArgumentPlaybook, args)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(result)
	case "list":
		result := listFiles("", *listArgumentPlaybookPath, *listArgumentPlaybookRegex)
		for _, str := range result {
			fmt.Println(str)
		}
	}

}
