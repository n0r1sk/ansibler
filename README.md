# AnsibleR -  RBAC enabled REST interface service for Ansible

The `R` in AnsibleR stands **R**emote or **R**EST.

![](logo/ansbiler-full-logo.png)

## What it does

`AnsibleR` is a REST service solution for Ansbile. `AnsibleR` is focused on simplicity. It is a simple Golang binary configured through a config file. The use of SSL ist mandatory, you have to provide certificates. The `RBAC` feature is provided via [Casbin](https://github.com/casbin/casbin).

## What it doesn't

There is no webinterface included. If you are in the need of, you have to write one on your own or you have to use a software that offers the abability to call http URL's. For example, you can easily integrate a `curl` call to `AnsibleR` into a GitLab pipelnie by using a Docker container that can issue `curl` requests.

# Usage

How to use AnsibleR

## General usage
~~~
usage: ansibler [<flags>] <command> [<args> ...]

A command-line ansible agent.

Flags:
  --help   Show context-sensitive help (also try --help-long and --help-man).
  --debug  Enable debug mode.

Commands:
  help [<command>...]
    Show help.

  server <config> [<port>]
    Enable sever mode.

  execute <playbookPath> <playbook> [<playbookargs>] [<regex>]
    Execute a command.

  list <playbookPath> [<regex>]
    List playbookfiles.
~~~

## IMPORTANT
AnsibleR currently uses a default regular expression pattern to find your scripts who in turn runs your playbook. This patter recognizes all scripts with a pattern  of `api-*.sh`.

## list Command
To list all your scripts that can be triggered by AnsibleR, run the follwing command.
~~~
$ ./ansibler list /home/ubuntu/ansible-r-basics/examples/demo 
api-01-simple-ping.sh
~~~

The directory `/home/ubuntu/ansible-r-basics/examples/` contains the following files.
~~~bash
$ ls /home/ubuntu/ansible-r-basics/examples/demo 
api-01-simple-ping.sh  run-01-simple-ping.sh  simple-ping.yml
~~~

As you can see, the default `api-*.sh` pattern matches the `api-01-simple-ping.sh` but **not** the `run-01-simple-ping.sh` script!

You can change this by running the following command, specifying the regular expression pattern.

~~~
$ ./ansibler list /home/ubuntu/ansible-r-basics/examples/demo "(?m)run-.*\.sh"
run-01-simple-ping.sh
~~~

## execute Command
You can execute a local script via the execeute command.
~~~
./ansibler execute /home/ubuntu/ansible-r-basics/examples/demo api-01-simple-ping.sh

PLAY [localhost] ***************************************************************

TASK [Ping] ********************************************************************
ok: [localhost]

PLAY RECAP *********************************************************************
localhost                  : ok=1    changed=0    unreachable=0    failed=0 
~~~

## server Command
The `server` Command will run AnsibleR as daemon which will provide a http based REST service to call the Ansible playbooks. There are some requirements which have to be fulfilled. 

### Certificates
AnsibleR needs (requires) certificates for SSL encryption. There is **NO** switch to disable this behavior currently. If you do not have certificates, you can create them for testing purposes with the[CFSSL](https://coreos.com/os/docs/latest/generate-self-signed-certificates.html) tool.

The following is an excerpt from the CFSSL homepage. Please head over there if this quick and dirty documentation is not working anymore. Please let us know if this is the case!

Download CFSSL via:
~~~
mkdir ~/bin 
curl -s -L -o ~/bin/cfssl https://pkg.cfssl.org/R1.2/cfssl_linux-amd64 
curl -s -L -o ~/bin/cfssljson https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64 
chmod +x ~/bin/{cfssl,cfssljson} 
export PATH=$PATH:~/bin
~~~

#### Create CA
We have to create a Certificate Authority(CA) before we can sign SSL certificates. First we need a `ca-config.json`:
~~~
{
    "signing": {
        "default": {
            "expiry": "43800h"
        },
        "profiles": {
            "server": {
                "expiry": "43800h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth"
                ]
            },
            "client": {
                "expiry": "43800h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "client auth"
                ]
            },
            "peer": {
                "expiry": "43800h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth",
                    "client auth"
                ]
            }
        }
    }
}
~~~

We need a `ca-csr.json`:
~~~
{
    "CN": "My own CA",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "US",
            "L": "CA",
            "O": "My Company Name",
            "ST": "San Francisco",
            "OU": "Org Unit 1",
            "OU": "Org Unit 2"
        }
    ]
}
~~~

Then we can create the CA:
~~~
cfssl gencert -initca ca-csr.json | cfssljson -bare ca -
~~~

Now generate the `server.json`, edit it (common names) and create the certificate:
~~~
$ cfssl print-defaults csr > server.json
$ cat server.json
{
    "CN": "example.net",
    "hosts": [
        "ansibler.m4r10k.cf"
    ],
    "key": {
        "algo": "ecdsa",
        "size": 256
    },
    "names": [
        {
            "C": "US",
            "L": "CA",
            "ST": "San Francisco"
        }
    ]
}
$ cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=server server.json | cfssljson -bare server

 cfssl certinfo -cert server.pem
{
  "subject": {
    "common_name": "example.net",
    "country": "US",
    "locality": "CA",
    "province": "San Francisco",
    "names": [
      "US",
      "San Francisco",
      "CA",
      "example.net"
    ]
  },
  "issuer": {
    "common_name": "My own CA",
    "country": "US",
    "organization": "My Company Name",
    "organizational_unit": "Org Unit 2",
    "locality": "CA",
    "province": "San Francisco",
    "names": [
      "US",
      "San Francisco",
      "CA",
      "My Company Name",
      "Org Unit 2",
      "My own CA"
    ]
  },
  "serial_number": "309255047637129319310765209478774080221703361007",
  "sans": [
    "ansibler.m4r10k.cf"
  ],
......

~~~

### Configure AnsibleR

Ansible needs some configuration files. The main `config.yaml` and in addition the Casbin RBAC policy model and definition.

#### AnsiblR config.yaml
AnsibleR needs a config file in yaml format, eg `config.yaml` with the following content. You have to configure all of the given paramenters.
~~~yaml
address: 172.16.18.223
port: 8443
casbinmodelfile: /home/ubuntu/ansibler-conf/casbin_model.conf
casbinpolicyfile: /home/ubuntu/ansibler-conf/casbin_policy.csv
certfile: /home/ubuntu/ansibler-conf/server.pem
keyfile: /home/ubuntu/ansibler-conf/server-key.pem
playbookbasepath: /home/ubuntu/ansible-r-basics/examples/
playbookregex: (?m)api-.*\.sh
~~~

#### AnsibleR casbin_model.conf and casbin_policy.csv
AnsibleR supports and requires RBAC configuration. There are two files. Firest the `casbin_model.conf`. This file represents the Casbin policy model we use. This file needs no change! Maybe we will embed it in the future.

~~~
[request_definition]
r = sub, obj, act

[policy_definition]
p = sub, obj, act

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
m = r.sub == p.sub && keyMatch(r.obj, p.obj) && regexMatch(r.act, p.act)
~~~

And second, which is important, the `casbin_model.conf`. This file defines the policy about what playbooks should be runnable by which API key.
~~~
p, fee30dd6-0625-4ae8-a9e7-48b900ecd5c4, /demo*, POST
~~~

The `fee30dd6-0625-4ae8-a9e7-48b900ecd5c4` is the API key, which is allowed to access the `/demo` path under the `playbookbasepath` configured in the `config.yaml` file.

## Test AnsibleR

The API key has to be submitted as a POST HEADER! If you just specify the folder, it will list the input.

~~~
curl -k -X POST -H "apikey: fee30dd6-0625-4ae8-a9e7-48b900ecd5c4" https://ansibler.m4r10k.cf:8443/demo/
{"Content":["api-01-simple-ping.sh","run-01-simple-ping.sh","simple-ping.yml"]}
~~~

If you submit the `.sh` filename too, it will execute the playbook.
~~~
curl -k -X POST -H "apikey: fee30dd6-0625-4ae8-a9e7-48b900ecd5c4" https://ansibler.m4r10k.cf:8443/demo/api-01-simple-ping.sh
{"Content":"\nPLAY [localhost] ***************************************************************\n\nTASK [Ping] ********************************************************************\nok: [localhost]\n\nPLAY RECAP *********************************************************************\nlocalhost                  : ok=1    changed=0    unreachable=0    failed=0   \n\n"}
~~~

If you try to run the `run-*.sh` script, you will retrieve a `{"Content":"Not an allowed script"}` because we configured the `playbookregex` option to only allow scripts that start with `api-*`.
~~~
curl -k -X POST -H "apikey: fee30dd6-0625-4ae8-a9e7-48b900ecd5c4" https://ansibler.m4r10k.cf:8443/demo/run-01-simple-ping.sh
{"Content":"Not an allowed script"}
~~~

You can also submit Payload data for your script!

First encode the argument as BASE64 string:
~~~
$ echo -n "Mariowashere" | base64
TWFyaW93YXNoZXJl
~~~

Put this into a JSON object:
~~~
{"arguments":["TWFyaW93YXNoZXJl"]}
~~~

And submit it as body to AnsibleR:
~~~
curl -k -X POST -H "apikey: fee30dd6-0625-4ae8-a9e7-48b900ecd5c4" -d '{"arguments":["TWFyaW93YXNoZXJl"]}' https://ansibler.m4r10k.cf:8443/demo-01/api-01-simple-ping.sh
{"Content":"\nPLAY [localhost] ***************************************************************\n\nTASK [Ping] ********************************************************************\nok: [localhost]\n\nTASK [Mesage] ******************************************************************\nok: [localhost] =\u003e {\n    \"msg\": \"Mariowashere\"\n}\n\nPLAY RECAP *********************************************************************\nlocalhost                  : ok=2    changed=0    unreachable=0    failed=0   \n\n"}
~~~


