module ansibler

go 1.13

require (
	github.com/Knetic/govaluate v3.0.1-0.20171022003610-9aa49832a739+incompatible
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf
	github.com/casbin/casbin v1.8.2
	github.com/gorilla/mux v1.7.1
	github.com/jasonlvhit/gocron v0.0.0-20190402024347-5bcdd9fcfa9b
	github.com/konsorten/go-windows-terminal-sequences v1.0.2
	github.com/sirupsen/logrus v1.4.2-0.20190403091019-9b3cdde74fbe
	golang.org/x/sys v0.0.0-20190415081028-16da32be82c5
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/yaml.v2 v2.2.2
)
