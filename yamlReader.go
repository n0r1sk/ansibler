/*
Copyright 2017 Mario Kleinsasser, Bernhard Rausch and Martin Steinwender

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

func loadYamlfile(configfile string, t interface{}) error {
	cfgdata, err := ioutil.ReadFile(configfile)

	if err != nil {
		log.Error("Cannot open config file from " + configfile)
		return err
	}

	err = yaml.Unmarshal([]byte(cfgdata), t)
	if err != nil {
		log.Error("Cannot map yml config file to interface, possible syntax error")
		return err

	}

	return nil
}
