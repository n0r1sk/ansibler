/*
Copyright 2017 Mario Kleinsasser, Bernhard Rausch and Martin Steinwender

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func checkServer() {
	log.Debug("Check Server")
	restart := loadConfig()

	if restart {
		err := httpServerInstance.Shutdown(context.Background())
		if err != nil {
			log.Panic(err)
		}
		go runServer()
	}

}

func startServer(configFilename string, port int) {
	log.Debug("Start Server")
	agentConfig = &agentConfigObj{configFilename: configFilename, Playbookregex: "none"}

	loadConfig()

	if agentConfig.Port <= 0 {
		agentConfig.portOverride = port
	}
	runServer()
}

func runServer() {
	port2Use := agentConfig.Port
	if agentConfig.portOverride > 0 {
		port2Use = agentConfig.portOverride
	}
	fmt.Printf("Running server '%s' on port '%d'...\n", agentConfig.Address, port2Use)
	r := buildRouter()
	httpServerInstance = &http.Server{Addr: fmt.Sprintf("%s:%d", agentConfig.Address, port2Use), Handler: r}

	err := httpServerInstance.ListenAndServeTLS(agentConfig.Certfile, agentConfig.Keyfile)
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(fmt.Sprintf("Error on server startup with following arguments \n%s \n%s \nresulting in: ", agentConfig.Certfile, agentConfig.Keyfile), err)
	}

}
