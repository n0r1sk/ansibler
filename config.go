/*
Copyright 2017 Mario Kleinsasser, Bernhard Rausch and Martin Steinwender

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import log "github.com/sirupsen/logrus"

func loadConfig() bool {
	restart := false
	loadYamlfile(agentConfig.configFilename, agentConfig)
	if agentConfig.Playbookregex == "none" {
		agentConfig.Playbookregex = defaultRegex
	}

	value, err := hashFileMD5(agentConfig.Certfile)
	if err != nil {
		log.Error(err)
	} else {
		restart = (agentConfig.certMD5 != "" && agentConfig.certMD5 != value)
		agentConfig.certMD5 = value

	}

	return restart
}
